# translation of plasma_applet_devicenotifier.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ravishankar Shrivastava <raviratlami@yahoo.com>, 2008.
# Ravishankar Shrivastava <raviratlami@aol.in>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2009-01-28 14:51+0530\n"
"Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>\n"
"Language-Team: Hindi <kde-i18n-doc@lists.kde.org>\n"
"Language: hne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 0.2\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr ""

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:44
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Click to safely remove all devices"
msgstr "ये उपकरन बर 1 काम"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgid "No removable devices attached"
msgstr "नवा उपकरन सूचक ल कान्फिगर करव"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "No devices plugged in"
msgid "No disks available"
msgstr "कोई उपकरन प्लगइन नइ हे"

#: package/contents/ui/main.qml:57
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgid "Most Recent Device"
msgstr "पिछला प्लग करे उपकरन: %1"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr ""

#: package/contents/ui/main.qml:207
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "नवा उपकरन सूचक ल कान्फिगर करव"

#: package/contents/ui/main.qml:233
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgid "Removable Devices"
msgstr "नवा उपकरन सूचक ल कान्फिगर करव"

#: package/contents/ui/main.qml:248
#, fuzzy, kde-format
#| msgid "Configure New Device Notifier"
msgid "Non Removable Devices"
msgstr "नवा उपकरन सूचक ल कान्फिगर करव"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr ""

#: package/contents/ui/main.qml:280
#, fuzzy, kde-format
#| msgid "No devices plugged in"
msgid "Show popup when new device is plugged in"
msgstr "कोई उपकरन प्लगइन नइ हे"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr ""

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr ""

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr ""

#: plugin/ksolidnotify.cpp:198
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "You are not authorized to mount this device."
msgstr "ये उपकरन बर 1 काम"

#: plugin/ksolidnotify.cpp:201
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "ये उपकरन बर 1 काम"

#: plugin/ksolidnotify.cpp:204
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "You are not authorized to eject this disc."
msgstr "ये उपकरन बर 1 काम"

#: plugin/ksolidnotify.cpp:211
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgid "Could not mount this device as it is busy."
msgstr "पिछला प्लग करे उपकरन: %1"

#: plugin/ksolidnotify.cpp:242
#, fuzzy, kde-format
#| msgid ""
#| "Cannot eject the disc.\n"
#| "One or more files on this disc are open within an application."
msgid "One or more files on this device are open within an application."
msgstr ""
"डिस्क ल बाहिर नइ कर सकिस.\n"
"कौनो अनुपरयोग मं एखर उपकरन के एकाधिक फाइल खुले होही."

#: plugin/ksolidnotify.cpp:244
#, fuzzy, kde-format
#| msgid ""
#| "Cannot unmount the device.\n"
#| "One or more files on this device are open within an application."
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] ""
"उपकरन ल अनमाउन्ट नइ कर सकिस.\n"
"कौनो अनुपरयोग मं एखर उपकरन के एक या अधिक फाइल खुले होही."
msgstr[1] ""
"उपकरन ल अनमाउन्ट नइ कर सकिस.\n"
"कौनो अनुपरयोग मं एखर उपकरन के एक या अधिक फाइल खुले होही."

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ""

#: plugin/ksolidnotify.cpp:265
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgid "Could not mount this device."
msgstr "पिछला प्लग करे उपकरन: %1"

#: plugin/ksolidnotify.cpp:268
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "पिछला प्लग करे उपकरन: %1"

#: plugin/ksolidnotify.cpp:271
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Could not eject this disc."
msgstr "ये उपकरन बर 1 काम"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "ये उपकरन बर 1 काम"
#~ msgstr[1] "ये उपकरन बर 1 काम"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to safely remove this device."
#~ msgstr "ये उपकरन बर 1 काम"

#, fuzzy
#~| msgid "<font color=\"%1\">Devices recently plugged in:</font>"
#~ msgid "Devices recently plugged in:"
#~ msgstr "<font color=\"%1\">हाल ही मं प्लग करे उपकरन:</font>"

#, fuzzy
#~| msgid " sec"
#~ msgid " second"
#~ msgid_plural " seconds"
#~ msgstr[0] "सेक."
#~ msgstr[1] "सेक."

#~ msgid "&Time to stay on top:"
#~ msgstr "सीर्स मं बने रहन के समय (&T) :"

#~ msgid "&Number of items displayed:"
#~ msgstr "प्रदर्सित करे जाय वाले चीज मन के संख्या (&N):"

#~ msgid "Unlimited"
#~ msgstr "असीमित"

#~ msgid "&Display time of items:"
#~ msgstr "चीज मन ल प्रदर्सित करे के समय (&D)"
