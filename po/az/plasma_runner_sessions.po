# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-06 01:40+0000\n"
"PO-Revision-Date: 2023-02-19 15:54+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: sessionrunner.cpp:20
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to log out of the "
"session"
msgid "logout;log out"
msgstr "çıxış;sesiyadan çıxış"

#: sessionrunner.cpp:23
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Cari istifadəçinin sesiyasından çıxır"

#: sessionrunner.cpp:26
#, fuzzy, kde-format
#| msgctxt ""
#| "KRunner keywords (split by semicolons without whitespace) to shut down "
#| "the computer"
#| msgid "shutdown;shut down"
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to shut down the "
"computer"
msgid "shutdown;shut down;power;power off"
msgstr "söndürmək;söndür"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Kompyuteri söndürmək"

#: sessionrunner.cpp:32
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to restart the "
"computer"
msgid "restart;reboot"
msgstr "yenidən başlatmaq;təkrar aç"

#: sessionrunner.cpp:35
#, kde-format
msgid "Reboots the computer"
msgstr "Kompyuteri yenidən başladır"

#: sessionrunner.cpp:39
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to lock the screen"
msgid "lock;lock screen"
msgstr "kilid;ekranı kilidləmək"

#: sessionrunner.cpp:41
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Cari sesiyanı kilidləyir və ekran qoruyucusunu işə salır"

#: sessionrunner.cpp:44
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to save the "
"desktop session"
msgid "save;save session"
msgstr "saxla:sesiyanı saxla"

#: sessionrunner.cpp:47
#, kde-format
msgid "Saves the current session for session restoration"
msgstr "Sesiyanı bərpa etməyək üçün cari sesiyanı saxlayır"

#: sessionrunner.cpp:50
#, kde-format
msgctxt ""
"KRunner keywords (split by semicolons without whitespace) to switch user "
"sessions"
msgid "switch user;new session"
msgstr "istifadəçini dəyişmək;yeni sesiya"

#: sessionrunner.cpp:53
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Yeni sesiyanı digər istifadəçi ilə başladır"

#: sessionrunner.cpp:56
#, kde-format
msgctxt "KRunner keyword to list user sessions"
msgid "sessions"
msgstr "sesiyalar"

#: sessionrunner.cpp:57
#, kde-format
msgid "Lists all sessions"
msgstr "bütün sesiyaların siyahısı"

#: sessionrunner.cpp:60
#, kde-format
msgctxt "KRunner keyword to switch user sessions"
msgid "switch"
msgstr "dəyişdir"

#: sessionrunner.cpp:62
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
":q: istifadəçisi sesiyasına keçirir, və ya əgər :q: göatərilməyibsə bütün "
"istifadəçiləri göstərir"

#: sessionrunner.cpp:81
#, kde-format
msgctxt "log out command"
msgid "Log Out"
msgstr "Sİstemdən çıx"

#: sessionrunner.cpp:89
#, kde-format
msgctxt "turn off computer command"
msgid "Shut Down"
msgstr "Söndür"

#: sessionrunner.cpp:96
#, kde-format
msgctxt "restart computer command"
msgid "Restart"
msgstr "Yenidən başlat"

#: sessionrunner.cpp:103
#, kde-format
msgctxt "lock screen command"
msgid "Lock"
msgstr "Kilidlə"

#: sessionrunner.cpp:110
#, kde-format
msgid "Save Session"
msgstr "Sesiyanı saxla"

#: sessionrunner.cpp:151
#, kde-format
msgid "Switch User"
msgstr "istifadəçini dəyiş"

#: sessionrunner.cpp:237
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type '%1')</li><li>Plasma widgets (such as the "
"application launcher)</li></ul>"
msgstr ""
"<p>Siz yeni iş masası sesiyasına daxil olmaq üzrəsiniz.</p><p>Giriş ekran "
"göstəriləcək və cari sesiya gizlədiləcəkdir.</p><p>Siz bunlardan istifadə "
"edərək iş masası sesiyalarını dəyişə bilərsiniz:</p><ul>Ctrl+Alt+F{sesiyanın "
"nömrəsi}<li></li>Plasma axtarışı (növ \"%1\")<li></li>Plasma vidjet (tətbiq "
"başladıcısı kimi)<li></li>"

#: sessionrunner.cpp:246
#, kde-format
msgid "New Desktop Session"
msgstr "Yeni iş masası sesiyası"

#~ msgctxt "log out command"
#~ msgid "logout"
#~ msgstr "Sesiyadan çıxış"

#~ msgctxt "lock screen command"
#~ msgid "lock"
#~ msgstr "kilidləmək"

#~ msgctxt "restart computer command"
#~ msgid "reboot"
#~ msgstr "yenidən başlatmaq"

#~ msgctxt "save session command"
#~ msgid "save"
#~ msgstr "saxla"

#~ msgctxt "switch user command"
#~ msgid "switch :q:"
#~ msgstr ":q: istifadəçisinə keçid"

#~ msgid "new session"
#~ msgstr "yeni sesiya"

#~ msgctxt "log out command"
#~ msgid "Logout"
#~ msgstr "Çıxış"

#~ msgid "Restart the computer"
#~ msgstr "Bu kompyuteri yenidən başlatmaq"

#~ msgctxt "shut down computer command"
#~ msgid "shutdown"
#~ msgstr "söndürmək"

#~ msgid "Shut down the computer"
#~ msgstr "Kompyuteri söndürmək"

#~ msgid "Save the session"
#~ msgstr "Sesiyanı saxlamaq"

#~ msgid "Save the current session for session restoration"
#~ msgstr "Sesiyanın bərpa edilməsi üçün cari sesiyanı saxlamaq"

#~ msgid "Warning - New Session"
#~ msgstr "Diqqət: - Yeni Sesiya"

#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Yeni İş masası sesiyasının açılmasını seçdiniz.<br />Yeni cari "
#~ "gizlədiləcək və yeni giriş ekranı göstəriləcək.<br />Hər sesiyaya uyğun F "
#~ "düyməsi var; qaydaya görə F%1 ilk sesiyaya uyğundur, F%2 ikinci sesiyaya "
#~ "uyğundur və s. Sesiyanı, Ctrl, Alt və uyğun F düyməsini eyni zamanda "
#~ "basmaqla dəyişə bilərsiniz. Həmçinin Plasma paneli və İş masası "
#~ "menyusunda sesiyanı dəyişmək əməli var.</p>"
