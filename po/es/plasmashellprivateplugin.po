# Spanish translations for plasmashellprivateplugin.po package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2014.
# SPDX-FileCopyrightText: 2014, 2015, 2016, 2018, 2020, 2021, 2022, 2023 Eloy Cuadra <ecuadra@eloihr.net>
msgid ""
msgstr ""
"Project-Id-Version: plasmashellprivateplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-05 00:39+0000\n"
"PO-Revision-Date: 2023-11-23 20:35+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net"

#: calendar/eventdatadecorator.cpp:50
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Días festivos"

#: calendar/eventdatadecorator.cpp:52
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Eventos"

#: calendar/eventdatadecorator.cpp:54
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Tareas pendientes"

#: calendar/eventdatadecorator.cpp:56
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Otros"

#: calendar/qml/DayDelegate.qml:74
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 evento"
msgstr[1] "%1 eventos"

#: calendar/qml/DayDelegate.qml:75
#, kde-format
msgid "No events"
msgstr "No hay eventos"

#: calendar/qml/MonthViewHeader.qml:73
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:129
#, kde-format
msgid "Days"
msgstr "Días"

#: calendar/qml/MonthViewHeader.qml:135
#, kde-format
msgid "Months"
msgstr "Meses"

#: calendar/qml/MonthViewHeader.qml:141
#, kde-format
msgid "Years"
msgstr "Años"

#: calendar/qml/MonthViewHeader.qml:179
#, kde-format
msgid "Previous Month"
msgstr "Mes anterior"

#: calendar/qml/MonthViewHeader.qml:181
#, kde-format
msgid "Previous Year"
msgstr "Año anterior"

#: calendar/qml/MonthViewHeader.qml:183
#, kde-format
msgid "Previous Decade"
msgstr "Década anterior"

#: calendar/qml/MonthViewHeader.qml:200
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Hoy"

#: calendar/qml/MonthViewHeader.qml:201
#, kde-format
msgid "Reset calendar to today"
msgstr "Reiniciar el calendario a hoy"

#: calendar/qml/MonthViewHeader.qml:212
#, kde-format
msgid "Next Month"
msgstr "Mes siguiente"

#: calendar/qml/MonthViewHeader.qml:214
#, kde-format
msgid "Next Year"
msgstr "Año siguiente"

#: calendar/qml/MonthViewHeader.qml:216
#, kde-format
msgid "Next Decade"
msgstr "Década siguiente"

#: calendar/qml/MonthViewHeader.qml:272
#, kde-format
msgid "Keep Open"
msgstr "Mantener abierto"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Configurar..."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Bloqueo de pantalla activado"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Define si se debe bloquear la pantalla tras el tiempo indicado."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Tiempo de espera del salvapantallas"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Indica los minutos tras los que se bloqueará la pantalla."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nueva sesión"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtros"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"Este elemento gráfico se escribió para una versión desconocida más antigua "
"de Plasma y no es compatible con Plasma %1. Póngase en contacto con el autor "
"del elemento gráfico para obtener una versión actualizada."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"Este elemento gráfico se escribió para Plasma %1 y no es compatible con "
"Plasma %2. Póngase en contacto con el autor del elemento gráfico para "
"obtener una versión actualizada."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"Este elemento gráfico se escribió para Plasma %1 y no es compatible con "
"Plasma %2. Actualice Plasma para poder usar este elemento gráfico."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""
"Este elemento gráfico se escribió para Plasma %1 y no es compatible con la "
"última versión de Plasma. Actualice Plasma para poder usar este elemento "
"gráfico."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr "Accesibilidad"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Lanzadores de aplicaciones"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr "Astronomía"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr "Fecha y hora"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr "Herramientas de desarrollo"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr "Educativos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Medio ambiente y clima"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr "Ejemplos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr "Sistema de archivos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Diversión y juegos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr "Gráficos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr "Idiomas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr "Mapas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Varios"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimedia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr "Servicios en línea"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr "Productividad"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr "Información del sistema"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr "Utilidades"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Ventanas y tareas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr "Portapapeles"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr "Tareas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Todos los elementos gráficos"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "Ejecutando"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "No se puede instalar"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Categorías:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Descargar nuevos elementos gráficos de Plasma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Instalar elemento gráfico desde un archivo local..."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Seleccionar archivo de plasmoide"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "La instalación del paquete %1 ha fallado."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Fallo de instalación"

#~ msgid "&Execute"
#~ msgstr "&Ejecutar"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Plantillas"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Consola para guiones del intérprete de órdenes del escritorio"

#~ msgid "Editor"
#~ msgstr "Editor"

#~ msgid "Load"
#~ msgstr "Cargar"

#~ msgid "Use"
#~ msgstr "Usar"

#~ msgid "Output"
#~ msgstr "Salida"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "No se puede cargar el archivo de script <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Abrir archivo de script"

#~ msgid "Save Script File"
#~ msgstr "Guardar archivo de script"

#~ msgid "Executing script at %1"
#~ msgstr "Ejecutando script en %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Tiempo de ejecución: %1 ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Descargar complementos de fondos del escritorio"

#~ msgid "Containments"
#~ msgstr "Contenedores"

#~ msgctxt ""
#~ "%1 is a type of widgets, as defined by e.g. some plasma-packagestructure-"
#~ "*.desktop files"
#~ msgid "Download New %1"
#~ msgstr "Descargar nuevo %1"
