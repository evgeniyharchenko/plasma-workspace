# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2010, 2014, 2020, 2021, 2022, 2024.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-26 00:39+0000\n"
"PO-Revision-Date: 2024-03-28 15:44+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: menu.cpp:102
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "Monstra KRunner"

#: menu.cpp:107
#, kde-format
msgid "Open Terminal"
msgstr "Aperi terminal"

#: menu.cpp:111
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Bloca schermo"

#: menu.cpp:120
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "Lassa ..."

#: menu.cpp:125
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Display Configuration"
msgstr "Configuration  de monstrator"

#: menu.cpp:285
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Configura Plugin de Menu Contextual"

#: menu.cpp:295
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[Altere actiones]"

#: menu.cpp:298
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Actiones de tapete de papiro"

#: menu.cpp:302
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[Separator]"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Configure Display Settings…"
#~ msgstr "Configura preferentias de monstrator..."

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "Executa Commando..."
